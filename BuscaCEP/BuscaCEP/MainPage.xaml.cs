﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace BuscaCEP
{
    public class Endereco
    {
        public string Cep { get; set; }
        public string Logradouro { get; set; }
        public string Complemento { get; set; }
        public string Bairro { get; set; }
        public string Localidade { get; set; }
        public string Uf { get; set; }
        public string Unidade { get; set; }
        public string Ibge { get; set; }
        public string EnderecoCompleto { get; set; }


    }

    [DesignTimeVisible(false)]
    public partial class MainPage : ContentPage
    {

        public Endereco Endereco { get; set; }

        public MainPage()
        {





            InitializeComponent();


            this.Endereco = new Endereco();
            //this.Endereco.Logradouro = "Testando";
            this.BindingContext = this;

        }

        public async void BuscarEndereco()
        {
            Console.WriteLine("iniciando consumo da api");
            Uri url = new Uri("http://viacep.com.br/ws/" + Endereco.Cep + "/json/");

            //call endpoint
            HttpResponseMessage httpResponse = await Services.HttpServices.GetRequest(url.AbsoluteUri);

            if (httpResponse.IsSuccessStatusCode)
            {
                string stringResponse = httpResponse.Content.ReadAsStringAsync().Result;
                Console.WriteLine("\n============");
                Console.WriteLine(stringResponse);

                Endereco Endereco = Services.SerializationService.DeserializeObject<Endereco>(stringResponse);

                Endereco End = Services.SerializationService.DeserializeObject<Endereco>(stringResponse);

                this.Endereco = End;
                OnPropertyChanged(nameof(this.Endereco));

                Console.WriteLine("\n ======Logradouro");
                Console.WriteLine(this.Endereco.Logradouro);
            }
        }

        private void Button_Clicked(object sender, EventArgs e)
        {
            BuscarEndereco();
        }
    }
}